{
    "id": "756e0806-27cd-4e59-8483-f442cbfc38df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "3d4cdb9b-3c89-43dd-a6b0-cddbd1b85b4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "756e0806-27cd-4e59-8483-f442cbfc38df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
    "visible": true
}