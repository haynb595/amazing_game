{
    "id": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "_playerdown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aae8ee5-6c5e-4d2d-b600-9e00372482a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "0b2027f0-ca2f-4515-ada6-0bc660be583a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aae8ee5-6c5e-4d2d-b600-9e00372482a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70a38f2d-1b81-40fd-bfe1-3f86926bee8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aae8ee5-6c5e-4d2d-b600-9e00372482a1",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "74dde16b-6c34-486c-af7a-a4d2658df216",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "67eff94e-7ab5-4130-8f34-c572aef35e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74dde16b-6c34-486c-af7a-a4d2658df216",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2579991-cd37-48fb-bf51-38c3e5dc1260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74dde16b-6c34-486c-af7a-a4d2658df216",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "36ee108b-b5a2-4d47-bed4-1890a396a194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "838e1d29-c5e8-415d-97fb-54ca29822d9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ee108b-b5a2-4d47-bed4-1890a396a194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a238b58-4772-4eb1-9c59-2c284a4347f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ee108b-b5a2-4d47-bed4-1890a396a194",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "1ed797ad-2b9f-486e-8f45-0a7292973626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "7b73af37-7b0e-4d82-b98e-f6a2ff215f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed797ad-2b9f-486e-8f45-0a7292973626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200ad998-a2cd-4a86-9195-dcc275997543",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed797ad-2b9f-486e-8f45-0a7292973626",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "f916add8-a0f7-45fc-8347-c0323a28c304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "cd7393ba-6bf7-4f73-8992-c3e8ec2f4852",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f916add8-a0f7-45fc-8347-c0323a28c304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b8a5bb-c5f0-4e60-aaf6-a462eb88c214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f916add8-a0f7-45fc-8347-c0323a28c304",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "f4f17f3d-727c-4069-b6ca-c433af452f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "f72f9fca-3dcd-4aa3-9c94-ddecb7b73410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4f17f3d-727c-4069-b6ca-c433af452f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb68804e-d48f-4682-b95f-0cd010248121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4f17f3d-727c-4069-b6ca-c433af452f7a",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "7cb2d849-013d-4ba2-9dae-47c787daf1af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "89039670-e903-4d09-bfbd-d2ac08adfa50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb2d849-013d-4ba2-9dae-47c787daf1af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "836156a3-5224-4530-9193-7781fadcec37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb2d849-013d-4ba2-9dae-47c787daf1af",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        },
        {
            "id": "0a2c8f42-0550-41bf-83a5-889fd5ac2aea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "compositeImage": {
                "id": "22cd2a0b-dd74-480f-a883-96b729df588c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2c8f42-0550-41bf-83a5-889fd5ac2aea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c16d42dd-ea4a-4139-b11a-91ad630ba0f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2c8f42-0550-41bf-83a5-889fd5ac2aea",
                    "LayerId": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "54be018a-d54f-4cb7-a0e7-f4f3842bd85d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4137f599-7ad6-4eea-ba43-85c34e39eb02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}